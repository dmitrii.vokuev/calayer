//
//  ThirdViewController.swift
//  CALayerDemo
//
//  Created by dmitrii on 09.09.19.
//  Copyright © 2019 Ivan Akulov. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    @IBAction func pressButton(_ sender: Any) {
       // showFourthScreen
        performSegue(withIdentifier: "showFourthScreen", sender: self)
    }
    

}
