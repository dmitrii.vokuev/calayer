//
//  SecondViewController.swift
//  CALayerDemo
//
//  Created by Ivan Akulov on 07/12/2016.
//  Copyright © 2016 Ivan Akulov. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, CAAnimationDelegate {
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.layer.cornerRadius = imageView.frame.height / 2
            imageView.layer.masksToBounds = true
            imageView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            imageView.layer.borderWidth = 5
        }
    }
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.layer.shadowOffset = CGSize(width: 0, height: 5)
            button.layer.shadowOpacity = 1
            button.layer.shadowRadius = 10
            button.layer.cornerRadius = 10
            button.layer.shadowColor = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
        }
    }
    
    var shapeLayer: CAShapeLayer! {
        didSet {
            shapeLayer.lineWidth = 10
            shapeLayer.lineCap = CAShapeLayerLineCap(rawValue: "round")
            shapeLayer.fillColor = nil
            shapeLayer.strokeEnd = 0
            let color = #colorLiteral(red: 0.9351197835, green: 1, blue: 0.1776672811, alpha: 1).cgColor
            shapeLayer.strokeColor = color
        }
    }
    
    
    override func viewDidLayoutSubviews() {
   //     gradientLayer.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        configShapeLayer(shapeLayer)
    }
    
    func configShapeLayer(_ shapeLayer: CAShapeLayer) {
        shapeLayer.frame = imageView.bounds
        let rect = CGRect(x: imageView.frame.minX, y: imageView.frame.minY, width: imageView.frame.width, height: imageView.frame.height)
        let circle = UIBezierPath(ovalIn: rect)

        shapeLayer.path = circle.cgPath
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shapeLayer = CAShapeLayer()
        view.layer.addSublayer(shapeLayer)
    }
    
    @IBAction func tapButton(_ sender: Any) {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = 1
        animation.duration = 1
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.fillMode = CAMediaTimingFillMode.both
        animation.isRemovedOnCompletion = true
        
        animation.delegate = self
        
        shapeLayer.add(animation, forKey: nil)
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag == true {
            performSegue(withIdentifier: "showThirdViewController", sender: self)
        }
    }
    
    
}
