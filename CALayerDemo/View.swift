//
//  View.swift
//  CALayerDemo
//
//  Created by dmitrii on 09.09.19.
//  Copyright © 2019 Ivan Akulov. All rights reserved.
//

import UIKit

@IBDesignable
class View: UIView {
    
    private var size = CGSize()
    @IBInspectable var cornerRadiiSize: CGFloat = 0 {
        didSet {
            size = CGSize(width: cornerRadiiSize, height: cornerRadiiSize)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let shapreLayer = CAShapeLayer()
        
        shapreLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .bottomRight], cornerRadii: size).cgPath
        
        layer.mask = shapreLayer
    }
    
    override func prepareForInterfaceBuilder() {
        setNeedsLayout()
    }
}
